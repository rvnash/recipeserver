#!/bin/bash -f

. ~/.bash_profile

echo "Starting Recipe Server"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

echo "Connecting to $DIR"

resolvedIP=$(nslookup "www.google.com" | awk -F':' '/^Address: / { matched = 1 } matched { print $2}' | xargs)
until [ -n "$resolvedIP" ]; do
    echo "Looking up www.google.com"
    resolvedIP=$(nslookup "www.google.com" | awk -F':' '/^Address: / { matched = 1 } matched { print $2}' | xargs)
done

# Runs server.js

while true; do
  ./server.js
  if [ $? == 0 ]; then
    echo "Getting latest code and restarting..."
    git pull
  else
    echo "Exit code: $?"
    sleep 30
    git pull
  fi
done
