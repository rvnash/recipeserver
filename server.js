#!/usr/bin/env node

/**
 * Module dependencies.
 */

var app = require('./app');
var http = require('http');
var https = require('https');
var fs = require('fs');

/**
 * Get port from environment and store in Express.
 */

var port = normalizePort(process.env.PORT || '3000');
app.set('port', port);
var portS = normalizePort(process.env.PORTS || '3443');
app.set('portS', portS);

/**
 * Create HTTPS server.
 */

var server = http.createServer(app);
var serverS = https.createServer({
    key: fs.readFileSync('server.key'),
    cert: fs.readFileSync('server.cert')
}, app);


/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

serverS.listen(portS);
serverS.on('error', onErrorS);
serverS.on('listening', onListeningS);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

function onError(error) {
  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error( 'Port ' + port + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error( 'Port ' + port + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  console.log('Listening on ' + bind);
}

function onErrorS(error) {
  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error( 'Port ' + portS + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error( 'Port ' + portS + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

function onListeningS() {
  var addr = serverS.address();
  var bind = 'port ' + addr.port;
  console.log('Listening on ' + bind);
}

require('./SyncImagesWithGoogle');
