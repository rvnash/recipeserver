# Recipe Server #

### How do I get set up? ###

* sudo apt-get install nodejs
* sudo apt-get install npm
* Install packages using npm
  * npm install babyparse@0.4.6 body-parser@1.15.2 cookie-parser@1.4.3 express@4.14.0 file-stream-rotator@0.0.7 google-auth-library@0.9.8 googleapis@12.0.0 jade@1.11.0 morgan@1.7.0 nodemailer@2.5.0 serve-favicon@2.3.0 -g

To configure server to run automatically on the server:
* Copy the file in this directory called "recipe.conf" to /etc/initd
* Edit the user name and directory location of the server in that file
* Restart the server, and the recipe server should be running

To control server on linux
  stop recipe
  restart recipe
  start recipe

To control server on OSX
  sudo launchctl unload /Library/LaunchDaemons/com.nash.recipeserver.plist
  sudo launchctl load -w /Library/LaunchDaemons/com.nash.recipeserver.plist

TODO: Describe how to create a recipe spreadsheet, give a sample, and tricks for formatting characters.

TODO: Describe how to get photos to work
