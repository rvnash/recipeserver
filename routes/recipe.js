var express = require('express');
var getter = express.Router();

var pug = require('pug');
var fnRender = pug.compileFile('views/recipe.pug');
var fnErrorRender = pug.compileFile('views/error.pug');

getter.get('/',
  function(req, res, next) {

    var title = req.query.title;
    var id = null;
    if (title == null) {
      id = req.query.ID;
    } else {
      results = global.rm.textSearch(null,title,'Title');
      if (results != null && results.length == 1) {
        id = results[0].ID;
      }
    }
    var hidePicts = req.query.hidePicts;
    if (hidePicts == 'true') {
      hidePicts = true;
    } else {
      hidePicts = false;
    }
    if (id == null) {
	var html = fnErrorRender( {
            message: 'No ID given',
            error: {}
	});
	res.send(html);
    } else {
      var recipe = global.rm.getRecipeByID(id);
      if (recipe == null) {
          var html = fnErrorRender( {
              message: 'No recipe with the ID\'' + id + '\'',
              error: {}
          });
	  res.send(html);
      } else {
          var html = fnRender(
                    {
                      recipe:recipe,
                      id:id,
                      pretty:true,
                      hidePicts: hidePicts
                    }
          );
          res.send(html);
      }
    }
  }
);

module.exports = getter;
